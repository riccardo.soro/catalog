# gitlab-agent

[GitLab Agent](https://docs.gitlab.com/ee/user/clusters/agent/) provides a two-way communication channel between your Kubernetes cluster and your GitLab group, project, or self-hosted instance.
